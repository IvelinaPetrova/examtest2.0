import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class CustomerGroupCheck {

    WebDriver driver;

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Iveto\\WebDrivers\\ChromeDriver\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://shop.pragmatic.bg/admin/");
    }
//@AfterMethod
//public  void tearDown(){
//        driver.quit();
//}
    @Test
    public void groupChange() {
        driver.findElement(By.cssSelector("input#input-username")).sendKeys("admin");
        driver.findElement(By.cssSelector("input#input-password")).sendKeys("parola123!");
        driver.findElement(By.cssSelector("button.btn-primary")).click();

        Assert.assertTrue(driver.getTitle().equalsIgnoreCase("Dashboard"));

        driver.findElement((By.cssSelector("li#menu-customer"))).click();
       driver.findElement(By.xpath("//ul[@id='collapse33']//a[text()='Customer Groups']")).click();
//        driver.findElement(By.cssSelector("ul#collapse33 a[href*='http://shop.pragmatic.bg/admin/index.php?route=customer/customer_group']")).click();
        driver.findElement(By.cssSelector("i.fa-pencil")).click();
        WebElement checkbox = driver.findElement(By.cssSelector("td.text-center input[value='26']"));
        checkbox.click();
        Assert.assertTrue(checkbox.isSelected());
        driver.findElement(By.cssSelector("div#content button[data-original-title='Delete']")).click();

        try {
            Alert alert = driver.switchTo().alert();
            alert.accept();
            WebElement massage = driver.findElement(By.cssSelector("div.alert-dismissible"));
            Assert.assertTrue(massage.getText().equals(" Success: You have modified customer groups!"));
        } catch (NoAlertPresentException e) {
            e.printStackTrace();
        }

        driver.findElement(By.cssSelector("div.container-fluid div.pull-right a[data-original-title='Add text']")).click();
        driver.findElement(By.cssSelector("div.input-group input")).sendKeys("Pragmatic");
        driver.findElement(By.cssSelector("div#content button[data-original-title='Save']")).click();
       driver.findElements(By.cssSelector("//div[@class='table-responsive']//td[text()='Pragmatic']"));
    }
}